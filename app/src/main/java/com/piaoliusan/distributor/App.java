package com.piaoliusan.distributor;

import android.app.Application;
import android.content.Context;

import com.piaoliusan.distributor.core.di.component.AppComponent;
import com.piaoliusan.distributor.core.di.component.DaggerAppComponent;
import com.piaoliusan.distributor.core.di.module.ApiModule;
import com.piaoliusan.distributor.core.di.module.DataManagerModule;

/**
 * Created by Deg on 7/23/17.
 */

public class App extends Application {

    public static Context context;
    private AppComponent mAppComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initDependencyInjection();
    }

    private void initDependencyInjection() {
        mAppComponent = DaggerAppComponent.builder()
                .apiModule(new ApiModule())
                .dataManagerModule(new DataManagerModule())
                .build();

        mAppComponent.inject(this);
    }


    public static Context getContext() {
        return context;
    }
}
