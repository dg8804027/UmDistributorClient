package com.piaoliusan.distributor.core.data.services;


import com.piaoliusan.distributor.core.data.BaseResponse;
import com.piaoliusan.distributor.core.net.NetUrl;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by Deg on 12/22/15.
 */
public interface UserService {


    @FormUrlEncoded
    @POST(NetUrl.login)
    Observable<BaseResponse> login(@Field("mobile") String mobile, @Field("verify_code") String verifyCode, @Field("xg_token") String token);


}