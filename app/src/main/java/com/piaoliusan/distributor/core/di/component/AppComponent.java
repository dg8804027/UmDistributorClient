package com.piaoliusan.distributor.core.di.component;

import com.piaoliusan.distributor.App;
import com.piaoliusan.distributor.base.BaseActivity;
import com.piaoliusan.distributor.base.BaseFragment;
import com.piaoliusan.distributor.core.di.module.ApiModule;
import com.piaoliusan.distributor.core.di.module.DataManagerModule;

import dagger.Component;

/**
 * Created by Deg on 12/15/15.
 */
@Component(modules = {ApiModule.class, DataManagerModule.class})
public interface AppComponent {

    void inject(BaseActivity baseActivity);

    void inject(BaseFragment baseFragment);

    void inject(App app);

}
