package com.piaoliusan.distributor.core.di.module;


import com.piaoliusan.distributor.core.data.services.UserService;
import com.piaoliusan.distributor.core.net.RetrofitAdapter;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
/**
 * Created by Deg on 12/15/15.
 */
@Module
public class ApiModule {


    @Provides
    UserService provideUserApi(Retrofit retrofit) {
        return retrofit.create(UserService.class);
    }

    @Provides
    Retrofit provideApiAdapter() {
        return RetrofitAdapter.getInstance();
    }
}
